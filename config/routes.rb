Rails.application.routes.draw do
  root 'home#index'

  get '/trial_exam', to: 'home#trial_exam'

  resources :morse, only: :index do
    get :start, on: :collection
  end

  resources :questions, only: :index do
    post :check, on: :member
    get :start, on: :collection
  end
end
