FROM ruby:3.1-alpine

RUN apk add --update tzdata && \
    cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

RUN apk add --update --virtual runtime-deps postgresql-client nodejs libffi-dev readline

WORKDIR /tmp
ADD Gemfile* ./

ENV RAILS_ENV=production \
    RACK_ENV=production

RUN apk add --virtual build-deps build-base openssl-dev postgresql-dev libc-dev linux-headers libxml2-dev libxslt-dev readline-dev && \
    bundle install --jobs=2 && \
    rails db:create && rails db:create && rails db:seed && \
    apk del build-deps

ENV APP_HOME /app
COPY . $APP_HOME
WORKDIR $APP_HOME

RUN RAILS_ENV=production bundle exec rake assets:precompile

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]

