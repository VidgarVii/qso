# frozen_string_literal: true

class TestMorseCode
  CODEBYLEVEL = [
    %w[а н],
    %w[а н и т]
  ].freeze

  def initialize(level: 0)
    @level = level.to_i
  end

  def start
    MorseCode.find_by(ru: CODEBYLEVEL[@level].sample.upcase).ru
  end
end
