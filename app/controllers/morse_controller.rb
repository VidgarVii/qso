# frozen_string_literal: true

class MorseController < ApplicationController
  def index
    @morse_code = MorseCode.all
  end

  def start
    cookies[:morse_code_level] ||= 0

    @code_by_level = ::TestMorseCode
      .new(level: cookies.fetch(:morse_code_level, 0))
      .start
  end
end
