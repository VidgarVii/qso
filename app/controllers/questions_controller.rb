# frozen_string_literal: true

class QuestionsController < ApplicationController
  def index
    @questions = Question.all
  end

  def start
    weighting_questions_cookies

    @question = Question.includes(:answers).find(start_question_id)
    @question_ids = Question.where(category: fetch_category).ids
  end

  def check
    @question = Question.find(params.require(:id))
    check_result = @question.correct_answer.id == params[:answer_id].to_i

    if check_result
      weighting_questions_cookies(id: @question.id, correct: true)
      category_params = params[:category] ? { category: params[:category] } : {}
      sleep 0.5
      redirect_to action: :start, **category_params and return
    else
      weighting_questions_cookies(id: @question.id, correct: false)
    end
  end

  private

  helper_method :weight_questions

  def start_question_id
    weight = weight_questions.keys.sort.detect { |k| k.to_i > rand(80) }
    ids = weight_questions.flat_map do |k, v|
      next if weight >= k

      v
    end.compact

    question_ids = Question.where(category: fetch_category).ids
    ((question_ids & ids) | question_ids).sample
  end

  def weighting_questions_cookies(**opts)
    cookies[:weight_questions] ||=
      Base64.encode64({ 80 => [], 70 => [], 50 => [*Question.ids], 40 => [], 30 => [] }.to_json)

    return if opts.empty?

    update_weight_questions(opts)
    cookies[:weight_questions] = Base64.encode64(weight_questions.to_json)
  end

  def update_weight_questions(opts)
    current_weight = weight_questions.detect { |_, v| v.include?(opts[:id]) }[0]
    weight_questions[current_weight].delete(opts[:id])

    if opts[:correct]
      weight_questions[weight_questions.keys.detect { |w| w < current_weight }]&.push(opts[:id])
    else
      weight_questions[weight_questions.keys.sort.detect { |w| w > current_weight }]&.push(opts[:id])
    end
  end

  def weight_questions
    @weight_questions ||= JSON.parse(Base64.decode64(cookies[:weight_questions]))
  end

  def fetch_category
    (params[:category].to_i..4) || [1, 2, 3, 4]
  end
end
