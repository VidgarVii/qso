# frozen_string_literal: true

class MorseCode < ActiveYaml::Base
  set_root_path 'app/models'
  set_filename 'morse_code'
end
