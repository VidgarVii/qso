# frozen_string_literal: true

class Question < ApplicationRecord
  belongs_to :topic

  has_many :answers, dependent: :destroy

  def correct_answer
    answers.correct.first
  end
end
