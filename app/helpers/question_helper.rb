module QuestionHelper
  COLOR_BY_STATUS = {
    '80' => 'red',
    '70' => 'l_red',
    '40' => 'l_green',
    '30' => 'green'
  }.freeze

  def status_question(id)
    COLOR_BY_STATUS[weight_questions.detect { |_, v| v.include?(id) }[0]]
  end
end
