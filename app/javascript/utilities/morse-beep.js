const lettersToMorse = {
  "а": "sl",
  "б": "lsss",
  "ц": "lsls",
  "д": "lss",
  "е": "s",
  "ф": "ssls",
  "г": "lls",
  "х": "ssss",
  "и": "ss",
  "й": "slll",
  "к": "lsl",
  "л": "slss",
  "м": "ll",
  "н": "ls",
  "о": "lll",
  "п": "slls",
  "щ": "llsl",
  "р": "sls",
  "с": "sss",
  "т": "l",
  "у": "ssl",
  "ж": "sssl",
  "в": "sll",
  "ь": "lssl",
  "ы": "lsll",
  "з": "llss",
  "ч": "llls",
  "ш": "llll",
  "э": "sslss",
  "ю": "ssll",
  "я": "slsl",
  "1": "sllll",
  "2": "sslll",
  "3": "sssll",
  "4": "ssssl",
  "5": "sssss",
  "6": "lssss",
  "7": "llsss",
  "8": "lllss",
  "9": "lllls",
  "0": "lllll",
  "?": "ssllss",
  "!": "llssll",
  ".": "ssssss"
}
let listeners = {"start":[], "stop":[]}

export default class MorseBeep {
  constructor(unit, gap) {
    this.unit = unit || 10
    this.gap = gap || 10

    this.sayer.on = function(key, fn){
      listeners[key] = listeners[key] || []
      listeners[key].push(fn)
    }
  }

  beep(num, cb){
    let type = num == this.unit ? "dit" : "dah"
    this.emit("start-beep", type)
    this.beeper()(this.gap*num, () => {
      this.emit("end-beep", type)
      setTimeout(cb, this.unit*this.gap)
    });
  }

  long(cb) {
    cb = cb || function(){}
    this.beep(3*this.unit, cb)
  }

  short(cb) {
    cb = cb || function(){}
    this.beep(this.unit, cb)
  }

  pause(cb) {
    setTimeout(cb, 2*this.unit*this.gap)
  }

  space(cb) {
    setTimeout(cb, 4*this.unit*this.gap)
  }

  parse(str,cb) {
    if(str.length) {
      var l = str[0]
      str = str.slice(1)
      if(l=="s") {
        this.short(() => {
          this.parse(str,cb)
        })
      }
      else{
        this.long(() => {
          this.parse(str, cb)
        })
      }
    }
    else { this.pause(cb) }
  }

  sayer(msg, cb) {
    cb = cb || function(){}
    msg = msg.toLowerCase()
    this.say(msg, cb)
  }

  say(msg, cb) {
    if(msg.length){
      let l = msg[0]
      this.emit("start-letter", l)
      msg = msg.slice(1);
      let code = lettersToMorse[l]
      if(code){
        this.parse(code, () => {
          this.emit("end-letter", l)
          this.say(msg, cb)
        });
      }
      else if(l==" "){
        this.space(() => {
          this.emit("end-letter", l)
          this.say(msg, cb)
        })
      }
      else {
        this.emit("end-letter", l)
        this.say(msg, cb)
      }
    }
    else{
      cb()
    }
  }

  emit(key, value) {
    listeners[key] = listeners[key] || []
    for(var i=0; i<listeners[key].length; i++){
      listeners[key][i](value)
    }
  }

  beeper() {
    let ctx = new(window.AudioContext || window.webkitAudioContext);
    return function (duration, cb) {
      duration = +duration

      if (typeof cb != "function") cb = () => {}

      let  osc = ctx.createOscillator()
      osc.type = 'sine'
      osc.frequency.value = 800
      osc.connect(ctx.destination)

      let start = osc.start ? osc.start.bind(osc) : osc.noteOn.bind(osc)
      let stop = osc.stop ? osc.stop.bind(osc) : osc.noteOff.bind(osc)
      start(0)

      setTimeout(function () {
        stop(0)
        cb()
      }, duration)
    }
  }
}
