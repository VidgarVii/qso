import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['form', 'startExamLink']

  choice() {
    let choiceCategory = this.formTarget.querySelector('input:checked').value
    let url = new URL(this.startExamLinkTarget.href)

    url.searchParams.set('category', choiceCategory)
    this.startExamLinkTarget.href = url.href
  }
}
