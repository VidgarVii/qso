// Import and register all your controllers from the importmap under controllers/*

import { application } from "controllers/application"
import QuestionController from "./question_controller"
import CategoryController from "./category_controller"
import MorseController from "./category_controller"
import TestController from "./test_morse_code_controller"

// Eager load all controllers defined in the import map under controllers/**/*_controller
import { eagerLoadControllersFrom } from "@hotwired/stimulus-loading"
eagerLoadControllersFrom("controllers", application)
eagerLoadControllersFrom("question", QuestionController)
eagerLoadControllersFrom("category", CategoryController)
eagerLoadControllersFrom("morse", MorseController)
eagerLoadControllersFrom("test-morse-code", TestController)

// Lazy load controllers as they appear in the DOM (remember not to preload controllers in import map!)
// import { lazyLoadControllersFrom } from "@hotwired/stimulus-loading"
// lazyLoadControllersFrom("controllers", application)
