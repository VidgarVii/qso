import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['correctAnswer']

  connect() {
    if (this.element.dataset.choiceAnswer) {
      this.element.elements[this.element.dataset.choiceAnswer].labels[0].classList.add('fail')
      this.correctAnswerTarget.classList.add('success')
    }
  }
}
