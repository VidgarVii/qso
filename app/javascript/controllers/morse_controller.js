import { Controller } from "@hotwired/stimulus"
import MorseBeeper from "utilities/morse-beep"

export default class extends Controller {
  connect() {
    this.beeper = new MorseBeeper(10,10)
  }

  play(event) {
    this.beeper.sayer(event.target.dataset.char)
  }
}
