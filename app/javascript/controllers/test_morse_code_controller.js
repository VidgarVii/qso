import { Controller } from "@hotwired/stimulus"
import MorseBeeper from "utilities/morse-beep"

export default class extends Controller {
  static values = { msg: String }

  connect() {
    console.log('start')

    this.beeper = new MorseBeeper(10,10)
  }

  start() {
    this.beeper.sayer('щсо')
  }
}
