# frozen_string_literal: true

FactoryBot.define do
  factory :question do
    topic

    title { Faker::Lorem.sentence }
    category { 1 }
    image {}
  end
end
