describe QuestionsController, type: :controller do
  describe 'GET /index' do
    before { get :index }

    it_behaves_like 'success'
  end

  describe 'GET /start' do
    let_it_be(:question1) { create :question, category: 1 }
    let_it_be(:question2) { create :question, category: 2 }
    let_it_be(:question3) { create :question, category: 3 }
    let_it_be(:question4) { create :question, category: 4 }

    let(:params) { {} }

    before { get :start, params: }

    it_behaves_like 'success'

    it { expect(assigns(:question)).to be_a(Question) }

    context 'when category params' do
      let(:params) { { category: 1 } }

      it_behaves_like 'success'
    end

    context 'with category 1' do
      let(:params) { { category: 1 } }

      it_behaves_like 'success'

      it { expect(assigns(:question).category >= 1).to be_truthy }
    end

    context 'with category 2' do
      let(:params) { { category: 2 } }

      it_behaves_like 'success'

      it { expect(assigns(:question).category >= 2).to be_truthy }
    end

    context 'with category 3' do
      let(:params) { { category: 3 } }

      it_behaves_like 'success'

      it { expect(assigns(:question).category >= 3).to be_truthy }
    end

    context 'with category 4' do
      let(:params) { { category: 4 } }

      it_behaves_like 'success'

      it { expect(assigns(:question)).to eq question4 }
    end
  end
end
