# frozen_string_literal: true

describe HomeController, type: :controller do
  describe 'GET /index' do
    before { get :index }

    it_behaves_like 'success'
  end
end
