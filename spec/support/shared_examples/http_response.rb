# frozen_string_literal: true

RSpec.shared_examples 'success' do
  it 'has ok status' do
    expect(response).to have_http_status(:ok)
  end
end
