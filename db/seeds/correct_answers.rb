class String
  def smart_to_i
    ch = self

    if ch.to_i.zero?
      %w[a b c d].index(self) + 1
    else
      to_i
    end
  end
end

result = File.readlines('db/seeds/fixtures/correct_answers.txt').flat_map do |line|
  line.chomp.delete('[]').split(' ').map(&:smart_to_i)
end.each_slice(2)

questions = Question.order(:id).includes(:answers)

result.each do |numbers|
  # numbers[0] - номер вопроса
  # numbers[1] - номер правильного ответа

  questions[numbers[0] - 1].answers.order(:id)[numbers[1] - 1].update!(correct: true)
end
