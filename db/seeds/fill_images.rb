images = Dir['app/assets/images/for_exam/*.*'].map { |path| path.split('/').last }

questions = Question.order(:id)

images.each do |file_name|
  questions[file_name.to_i - 1].update!(image: file_name)
end
