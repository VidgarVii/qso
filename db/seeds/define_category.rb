questions = Question.order(:id)

CATA = {
  1 => [1..426],
  2 => [1..38, 47..98, 100..374, 387..426],
  3 => [1..34, 47..98, 100..135, 150..226, 387..391, 409..422],
  4 => [1..17, 47..98, 100..135, 150..226, 387..391, 409..422]
}.freeze

def fetch_category(index)
  CATA.select { |_k, v| v.flat_map(&:to_a).include?(index) }.keys.max
end

questions.each_with_index do |question, index|
  question.update!(category: fetch_category(index + 1))
end
